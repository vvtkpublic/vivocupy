const serverURL = 'vivoaigrpc.bestvvtk.com:50051'

var PROTO_PATH = __dirname + '/occupancy.proto';
var grpc = require('@grpc/grpc-js');
var protoLoader = require('@grpc/proto-loader');
var packageDefinition = protoLoader.loadSync(PROTO_PATH);
var Occupancy = grpc.loadPackageDefinition(packageDefinition).Occupancy;
var client = new Occupancy.Occupancy(serverURL, grpc.credentials.createInsecure());

function main() {
    // you can change radar to your device token
    var roomConfig = {
        token: ["radar"]
    }
    var call = client.Status(roomConfig);
    call.on('data', function (room) {
        console.log(room);
    });
}

main();